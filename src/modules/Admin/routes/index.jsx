import { Route, Routes } from "react-router-dom"
import { Courses } from "./Courses/AllCourses"

export const AdminRoutes = () => {

    return (
        <Routes >
            <Route path="*" element={<Courses />}/>
            <Route path="courses" element={<Courses />}/> 
        </Routes>
    )
}