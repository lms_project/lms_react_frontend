import { Button } from "@chakra-ui/react";
import edit from "../../../../../assets/Icons/edit.svg"
import { useNavigate } from "react-router-dom";
import { useCourses } from "../../../../../services/api/courses/courses.service";


export const useCoursesProps = () => {

    const courses = useCourses()

    console.log(courses?.data)

    const columns = [
    {
        title: '№',
        dataIndex: 'number',
        key: 'number',
        width: 48,
    },
    {
        title: 'Название курса',
        dataIndex: 'name',
        key: 'name',
        width: 220,
    },
    {
        title: 'Описание',
        dataIndex: 'for_who',
        key: 'for_who',
        width: 210,
    },
    {
        title: 'Тип',
        dataIndex: 'type',
        key: 'type',
        width: 88,
    },
    {
        title: 'Время',
        dataIndex: 'weekly_number',
        key: 'weekly_number',
        width: 100,
    },
    {
        title: 'Кол-во документов',
        dataIndex: 'number_of_materials',
        key: 'number_of_materials',
        width: 170,
    },
    {
        title: 'Дата',
        dataIndex: 'beginning_date_course',
        key: 'beginning_date_course',
        width: 124,
    },
    {
        title: 'Цена',
        dataIndex: 'grade',
        key: 'grade',
        width: 94,
      },
      {
        title: '',
        key: 'operations',
        render: (item) => {
          return (
            <div>
              <Button  
                padding="4px" 
                colorScheme="transparent" 
                onClick={() => useNavigate(`/admin/courses/${item?.id}`)}>
                <img src={edit} width={20} height={20} alt="edit" />
              </Button>
            </div>
          );
        },
      },
    ];

    const data = courses?.data?.courses.map(course => {
      return{
        name: course?.name,
        for_who: course?.description,
        type: course?.category,
        weekly_number: course?.number_of_lessons_per_week,
        number_of_materials: course?.lesson_duration,
        beginning_date_course: course?.created_at,
        grade: course?.price,
        key: course?.id
      }
    })


    return {
        columns,
        data,
    }
}