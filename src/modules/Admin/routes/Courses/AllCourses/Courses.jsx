import { Box } from "@chakra-ui/react"
import { useCoursesProps } from "./useCoursesProps"
import { CustomTable } from "../../../../../components/CustomTable/CustomTable"


export const Courses = () => {
    const {columns, data} = useCoursesProps()

    return <Box padding="0px 20px 0px 20px" marginTop="20px">
        <CustomTable columns={columns} data={data}/>
    </Box>
}