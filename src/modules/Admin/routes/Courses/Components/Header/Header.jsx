import style from "./style.module.scss"
import search from "../../../../../../assets/Icons/search.svg"
import lantern from "../../../../../../assets/Icons/lantern.svg"
import add from "../../../../../../assets/Icons/add.svg"
import { Container } from "../Container/Container"

export const Header = () => {

    return (
      <Container>
          <header className={style.header}>
                <h1 className={style.title}>Обучение</h1 >
                <div className={style.wrapper}>
                    <button>
                        <img src={search} alt="search-icon" width={36} height={36} />
                    </button>
                    <button>
                        <img src={lantern} alt="lantern-icon" width={36} height={36} />
                    </button>
                    <button className={style.btn}>
                        <img src={add} alt="add-icon" width={20} height={20} />
                        <span className={style.btnText}>Добавить</span>
                    </button>
                </div>
            </header>
      </Container>
    )
}