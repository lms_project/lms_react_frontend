import style from "./style.module.scss"

export const MainContainer = ({children}) => {

    return (
        <div className={style.container}>
            {children}
        </div>
    )
}