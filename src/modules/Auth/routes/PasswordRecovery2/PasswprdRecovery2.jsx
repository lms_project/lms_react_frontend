import { Container } from "../../../../components/Container/Container"
import { LMS } from "../../../../components/LMS/LMS"
import style from "./style.module.scss"

export const PasswordRecovery2 = () => {

    return (
        <>
            <Container>
               <div className={style.article}>
                    <LMS />
                    <div className={style.signIn}>
                        <div className={style.signInWrapper}>
                            <h2 className={style.signInTitle}>Восстановление пароля</h2>
                            <form className={style.signInForm}>
                                <span className={style.signInFormText}>Email или номер телефона</span>
                                <input className={style.signInFormInput} type="email" placeholder="Введите e-mail"/>
                                <span className={style.signInFormText}>Код активации</span>
                                <input className={style.signInFormInput} type="number" placeholder="1 2 3"/>
                                <p className={style.text}>Отправить код еще раз</p>   
                                <button className={style.signInBtn} type="submit">
                                    <span className={style.signInBtnText}>Получить код активации</span>
                                </button>
                            </form>
                        </div>
                    </div>
               </div>
            </Container>
        </>
    )
}