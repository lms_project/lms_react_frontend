import { useForm } from "react-hook-form"
import { useForgetPasswordVerification } from "../../../../services/api/auth/auth.service"

const useForgetPassword2Props =() => {
    const { register, handleSubmit } = useForm()
    const verification = useForgetPasswordVerification()
    const onSubmit = (data) => {
        verification.mutate({email: data.email, code: data.code})
    }
    return{
        register,
        handleSubmit,
        onSubmit
    }
}

export default useForgetPassword2Props