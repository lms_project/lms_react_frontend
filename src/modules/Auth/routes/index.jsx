import { Route, Routes } from "react-router-dom"
import { Login } from "../routes/Login"
import { Register } from "../routes/Register"
import { PasswordRecovery } from "../routes/PasswordRecovery"
import { PasswordRecovery2 } from "../routes/PasswordRecovery2"
import { Register2 } from "./Register2/Register2"
import ChangePassword from "./ChangePassword/ChangePassword"


const AuthRoutes = () => {
    return (
        <Routes>
            <Route path="*" element={<Login />} />
            <Route path="login" element={<Login />} />
            <Route path="register" element={<Register />} />
            <Route path="register2" element={<Register2 />} />
            <Route path="password-recovery" element={<PasswordRecovery />} />
            <Route path="password-recovery2" element={<PasswordRecovery2 />} />
            <Route path="create-password" element={<ChangePassword />} />
        </Routes>
    )
}

export default AuthRoutes
