import { useForm } from "react-hook-form"
import { useLogin } from "../../../../services/api/auth/auth.service"

const useLoginProps = () => {
    const { register, handleSubmit } = useForm()

    const login = useLogin()

    const onSubmit = (data) => {
        login.mutate({
            email: data.email,
            password: data.password
        })
        console.log(data)
    }

    return {
        register,
        handleSubmit,
        onSubmit 
    }
}

export default useLoginProps
