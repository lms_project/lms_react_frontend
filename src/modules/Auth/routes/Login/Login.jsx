import { Link, useNavigate } from "react-router-dom"
import { Container } from "../../../../components/Container/Container"
import { LMS } from "../../../../components/LMS/LMS"
import style from "./style.module.scss"
import useLoginProps from "./useLoginProps"

export const Login = () => {
    const navigate = useNavigate()
    const { register, handleSubmit, onSubmit } = useLoginProps()
    return (
        <>
           <Container>
               <div className={style.article}>
                    <LMS />
                    <div className={style.signIn}>
                        <div className={style.signInWrapper}>
                            <h2 className={style.signInTitle}>Вход в платформу</h2>
                            <form className={style.signInForm} onSubmit={handleSubmit(onSubmit)}>
                                <span className={style.signInFormText}>Email или номер телефона *</span>
                                <input className={style.signInFormInput} type="email" placeholder="Введите  e-mail" {...register("email")}/>
                                <span className={style.signInFormText}>Пароль *</span>
                                <input className={style.signInFormInput} type="password" placeholder="Введите пароль" {...register("password")}/>
                                <Link to="/auth/password-recovery" className={style.forgotPassword}>Забыли пароль?</Link>
                                <button className={style.signInBtn} type="submit">
                                    <span className={style.signInBtnText}>Войти</span>
                                </button>
                            </form>
                            <button className={style.signInRegister} onClick={() => navigate("/auth/register")}>
                                <span className={style.signInRegisterText}>Зарегистрироваться</span>
                            </button>
                        </div>
                    </div>
               </div>
           </Container>
        </>
    )
}

