import { useForm } from "react-hook-form"
import { useSendCode } from "../../../../services/api/auth/auth.service"

const usePasswordProps = () => {
    const { register, handleSubmit} = useForm()
    const sendCode = useSendCode()
    const onSubmit = (data) => {
        console.log(data)
        sendCode.mutate({email: data.email})
    }
    return{
        register,
        handleSubmit,
        onSubmit
    }
}

export default usePasswordProps