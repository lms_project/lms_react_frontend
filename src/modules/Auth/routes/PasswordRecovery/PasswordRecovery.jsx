import { useNavigate } from "react-router-dom"
import { Container } from "../../../../components/Container/Container"
import { LMS } from "../../../../components/LMS/LMS"
import style from "./style.module.scss"
import usePasswordProps from "./usePasswordProps"

export const PasswordRecovery = () => {
    const navigate = useNavigate()
    const { register, handleSubmit, onSubmit } = usePasswordProps()
    return (
        <>
            <Container>
               <div className={style.article}>
                    <LMS />
                    <div className={style.signIn}>
                        <div className={style.signInWrapper}>
                            <h2 className={style.signInTitle}>Восстановление пароля</h2>
                            <form className={style.signInForm} onSubmit={handleSubmit(onSubmit)}>
                                <span className={style.signInFormText}>Email или номер телефона</span>
                                <input className={style.signInFormInput} type="email" placeholder="Введите e-mail" {...register("email")}/>
                                <p className={style.text}>Введите номер телефона чтобы получить код активации</p>
                                <button onSubmit={() => navigate("/auth/register2")} className={style.signInBtn} type="submit">
                                    <span className={style.signInBtnText}>Получить код активации</span>
                                </button>
                            </form>
                        </div>
                    </div>
               </div>
            </Container>
        </>
    )
}