import { Container } from "../../../../components/Container/Container"
import { LMS } from "../../../../components/LMS/LMS"
import style from "./style.module.scss"
import stepImg from "../../../../assets/Icons/Register_step.png"
import useRegisterProps from "./useRegisterProps"
// import { Link } from "react-router-dom"

export const Register = () => {
    const { register, handleSubmit, onSubmit } = useRegisterProps()
    return (
        <>
            <Container>
                <div className={style.article}>
                    <LMS />
                    <div className={style.signIn}>
                        <div className={style.signInWrapper}>
                            <h2 className={style.signInTitle}>Пройдите регистрацию</h2>
                            <span>
                                <img src={stepImg} alt="register-step" />
                            </span>
                            <form className={style.signInForm} onSubmit={handleSubmit(onSubmit)}>
                                <span className={style.signInFormText}>Ваше Ф.И.О *</span>
                                <input className={style.signInFormInput} type="text" placeholder="Арина" {...register("first_name")}/>
                                <input className={style.signInFormInput} type="text" placeholder="Соколова" {...register("last_name")}/>
                                <span className={style.signInFormText}>Укажите в e-mail*</span>
                                <input className={style.signInFormInput} type="email" placeholder="Введите  e-mail" {...register("email")}/>
                                <span className={style.signInFormText}>Пароль *</span>
                                <input className={style.signInFormInput} type="password" placeholder="Введите пароль" {...register("password")}/>
                                <button className={style.signInBtn} type="submit">Следующий шаг</button>
                            </form>
                        </div>
                    </div>
                </div>
            </Container>
        </>
    )
}