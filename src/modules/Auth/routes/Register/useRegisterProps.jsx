import { useForm } from "react-hook-form"
import { useRegister, useSendCode } from "../../../../services/api/auth/auth.service"
import { useNavigate } from "react-router-dom"

const useRegisterProps = () => {

    const navigate = useNavigate()

    const { register, handleSubmit } = useForm()

    const registration = useRegister()

    const sendCode = useSendCode()

    console.log(registration.isSuccess)

    const onSubmit = (data) => {
        console.log(data)
        registration.mutate(data)
        registration.isSuccess? sendCode.mutate({email: data.email}) && navigate("/auth/register2") : null
    }

    return {
        register,
        handleSubmit,
        onSubmit
    }
}

export default useRegisterProps