import { Container } from "../../../../components/Container/Container"
import { LMS } from "../../../../components/LMS/LMS"
import style from "./style.module.scss"
import stepImg2 from "../../../../assets/Icons/Register-step2.png"
import { Checkbox } from "@chakra-ui/react"
import { useNavigate } from "react-router-dom"

export const Register2 = () => {
    const navigate = useNavigate()
    return (
        <>
            <Container>
               <div className={style.article}>
                    <LMS />
                    <div className={style.signIn}>
                        <div className={style.signInWrapper}>
                            <h2 className={style.signInTitle}>Пройдите регистрацию</h2>
                            <span>
                                <img src={stepImg2} alt="register-step" />
                            </span>
                            <form className={style.signInForm}>
                                <span className={style.signInFormText}>Код потвреждения</span>
                                <input className={style.signInFormInput} type="password" placeholder="Введите пароль"/>
                                <div className={style.checkbox}>
                                    <Checkbox className={style.checkboxText}>Нажимая кнопку «Зарегистрироваться», вы даёте согласие на обработку данных</Checkbox>
                                </div>
                                <button onClick={() => navigate("/auth/create-password")} className={style.signInBtn} type="submit">
                                    <span className={style.signInBtnText}>Подвердить код</span>
                                </button>
                            </form>
                        </div>
                    </div>
               </div>
            </Container>
        </>
    )
}