import { useVerification } from "../../../../services/api/auth/auth.service"

const useRegister2Props = () => {
    const verification = useVerification()
    const onClick = () => {
        verification.mutate({email: "email"})
    }
    return { onClick }
}

export default useRegister2Props