import style from "./style.module.scss"
import { LMS } from "../../../../components/LMS/LMS"
import { Container } from "../../../../components/Container/Container"


const ChangePassword = () => {
    return (
        <>
            <Container>
                <div className={style.article}>
                        <LMS />
                        <div className={style.signIn}>
                            <div className={style.signInWrapper}>
                                <h2 className={style.signInTitle}>Изменить пароль</h2>
                                <form className={style.signInForm}>
                                    <span className={style.signInFormText}>Введите пароль</span>
                                    <input className={style.signInFormInput} type="password" placeholder="Введите пароль"/>
                                    <button className={style.signInBtn} type="submit">Подвердить код</button>
                                </form>
                            </div>
                        </div>
                </div>
            </Container>
         </>
    )
}

export default ChangePassword