import { BrowserRouter } from "react-router-dom"
import { QueryClientProvider } from "react-query"
import queryClient from "./services/queryClient"
import Router from "./router"
import { ChakraProvider } from "@chakra-ui/react"



function App() {

  return (
    <ChakraProvider>
      <BrowserRouter>
        <QueryClientProvider client={queryClient}>
          <Router />
        </QueryClientProvider>
    </BrowserRouter>
    </ChakraProvider>
  )
}

export default App