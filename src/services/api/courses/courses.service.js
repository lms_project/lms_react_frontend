import { useQuery } from "react-query";
import request from "../../httpRequest";

const coursesServices = {
    courses: () => request.get('/courses?offset=0&limit=10').then(res => res.data)
}

export const useCourses = () => {
    return useQuery({queryKey: ["course"], queryFn: coursesServices.courses})
}