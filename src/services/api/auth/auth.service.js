import { useMutation } from "react-query";
import request from "../../httpRequest/index.js";
import { authStore } from "../../../store/auth.store.js";

const authServices = {
    login: () => request.post(`/login?email=${authStore.email}&password=${authStore.password}`)
        .then(res => {
            if(res.status === 200){
                alert("You have successfully logged in")
            }
            return res.data
        }),
    register: () => request.post(`/register`).then(res => {
        if(res.status === 200){
            console.log(res.data)
            authStore.userData = res.data
            alert("You have successfully registered. Please, check your email")
        }else if(res.status === 500){
            alert("Try again")
        }
    }),
    sendCode: (data) => request.post(`/sendcode?email=${data.email}`).then(res => {
        if(res.status === 200){
            console.log(res.data)
            alert("Check your email")
        }else if(res.status === 500){
            alert("Try again")
        }
        }
    ),
    ChangePassword: () => request.put(`/changepassword?email=${authStore.email}&password=${authStore.password}`),
    verifyCode: (data) => request.post(`/verify?email=${data.email}&code=${data.code}`).then(res => {
        if(res.status === 200){
            alert("You have successfully changed your password")
        }
    })
}

export const useLogin = () => {
    return useMutation({mutationKey: "login", mutationFn: authServices.login})
}

export const useRegister = () => {
    return useMutation({mutationKey: "register", mutationFn: authServices.register})
}

export const useSendCode = () => {
    return useMutation({mutationKey: "sendCode", mutationFn: authServices.sendCode})
}

export const useChangePassword = () => {
    return useMutation({mutationKey: "changePassword", mutationFn: authServices.ChangePassword})
}

export const useVerifyCode = () => {
    return useMutation({mutationKey: "verifyCode", mutationFn: authServices.verifyCode})
}