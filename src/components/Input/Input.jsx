import { Box } from "@chakra-ui/react"
import mail from "../../assets/mail_outline.svg"

export const Input = () => {

    return (
        <Box>
            <img src={mail} alt="" />
            <input type={"email"} />
            
        </Box>
    )
} 