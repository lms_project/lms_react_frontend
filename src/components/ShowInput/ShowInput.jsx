import React from "react"
import { Box, Button, Input, InputGroup, InputRightElement } from "@chakra-ui/react"
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons"
import style from "./style.module.scss"
import mail from "../../assets/mail_outline.svg"

export const ShowInput = () => {

    const [show, setShow] = React.useState(false)
    const handleClick = () => setShow(!show)
      
    return (
        <Box className={style.wrapper}  >
            <img src={mail} alt="mail-icon" />
            <InputGroup className={style.input} size='md'>
                <Input
                    // className={style.inputChakra}
                    border="none"
                    type={show ? 'text' : 'password'}
                    placeholder=  'Enter password' marginLeft="25px"
                />
                <InputRightElement width='4.5rem'>
                    <Button alignItems="center" h='1.75rem' size='sm' onClick={handleClick}>
                        {show ? <ViewIcon/>: <ViewOffIcon/>}
                    </Button>
                </InputRightElement>
            </InputGroup>
        </Box>
    )
    
}