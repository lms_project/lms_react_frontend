import systemImg from "../../assets/images/system.png"
import style from "./style.module.scss"

export const LMS = () => {

    return (
        <div className={style.lms}>
            <h2 className={style.title} >Learning Management system</h2>
            <img className={style.img} src={systemImg} alt="system-Image" width="672px" height="448px"/>
        </div>
    )
}