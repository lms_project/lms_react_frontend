import style from "./style.module.scss"
import clsx from "clsx";
import Table from "rc-table";


export const CustomTable = ({columns = [], data = [], className, ...props }) => {

    return <Table className={clsx(style.table, className)} columns={columns} data={data} {...props} />;
}