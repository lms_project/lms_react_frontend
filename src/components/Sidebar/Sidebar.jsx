import { Link } from "react-router-dom";
import style from "./style.module.scss"
import { useSidebarProps } from "./useSaidbarProps"
import frame from "../../assets/Icons/Frame 57409.svg"
import avatar from "../../assets/Icons/Avatar w. photo.svg"


export const Sidebar = () => {
    const { navList } = useSidebarProps()

    return(
        <div className={style.sidebar}>
            <div className={style.top}>
                <Link className={style.logo} to={"/courses"} >LMS</Link>
                <button>
                    <img src={frame} alt="icon" width={32} height={32}/>
                </button>
            </div>
            <nav className={style.navbar}>
                <ul className={style.list}>
                    {
                        navList.map((item, index) => (
                            <li className={style.item} key={index}> <img src={item.src} alt="" /> <Link to={item.path}>{item.name}</Link> </li>
                        ))
                    }
                </ul>
            </nav>
            <div className={style.bottom}>
                <img src={avatar} alt="avatar" />
                <div className={style.userData}>
                    <span className={style.userName}>Jane Cooper</span>
                    <span className={style.userEmail}>jane.cooper@email.com</span>
                </div>
            </div>
        </div>
    )
}