import school from "../../assets/Icons/school.svg"
import people from "../../assets/Icons/people.svg"

export const useSidebarProps = () => {
    

    const navList = [
        {
            src: school,
            path: "/courses",
            name: "Курсы",
        },
        {
            src: people,
            path: "/users",
            name: "Пользователи",
        }
    ]

    return { navList }
}