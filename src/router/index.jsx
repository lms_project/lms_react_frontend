import { Routes, Route, Navigate } from "react-router-dom"
import { observer } from "mobx-react-lite"
import { authStore } from "../store/auth.store"
import AuthRoutes from "../modules/Auth/routes"
import { MainLayout } from "../Layouts/MainLayout"
import { AdminRoutes } from "../modules/Admin/routes"

const Router = observer(() => {

    const isAuth = authStore.isAuth

    const role = "admin"

    if(!isAuth){
        return (
            <Routes>
                <Route path="auth/*" element={<AuthRoutes />} />
                <Route path="/*" element={<Navigate to="/auth/login" />} />
            </Routes>
                
        )
    }

    if (role === "admin") {
        return <Routes>
            <Route path="" element={<MainLayout />} >
                <Route  index path="/courses/*" element={<AdminRoutes />}/>
            </Route>
        </Routes>
    }
})

export default Router