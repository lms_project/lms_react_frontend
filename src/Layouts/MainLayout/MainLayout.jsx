import { Box } from "@chakra-ui/react"
import { Sidebar } from "../../components/Sidebar/Sidebar"
import { Outlet } from "react-router-dom"
import { Header } from "../../modules/Admin/routes/Courses/Components/Header/Header"
import { Container } from "../../modules/Admin/routes/Courses/Components/Container/Container"


export const MainLayout = () => {

    return <>
        <Header/>
        <Box display="flex">
            <Sidebar />
            <Container>
                <Box >
                    <Outlet />
                </Box>
            </Container>
        </Box>
    </>
}